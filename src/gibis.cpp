#include "gibis.hpp"
#include <iostream>
#include <fstream>

//--------------------------Builders--------------------------------------------
Gibis::Gibis(){
  tipo = "Generico";
  edicao = "Generico";
  volume = 0;
}
Gibis::Gibis(string editora, string titulo, int quantidade, int codigo, float preco){
  set_editora(editora);
  set_titulo(titulo);
  set_quantidade(quantidade);
  set_codigo(codigo);
  set_preco(preco);
}
Gibis::Gibis(string editora, string titulo, int quantidade, int codigo, float preco, string tipo, string edicao, int volume){
  set_tipo(tipo);
  set_edicao(edicao);
  set_volume(volume);
  set_editora(editora);
  set_titulo(titulo);
  set_quantidade(quantidade);
  set_codigo(codigo);
  set_preco(preco);
}
//--------------Destructor------------------------------------------------------
Gibis::~Gibis(){
  cout << "destrutor da classe" << endl;
}
//-----------------SET----------------------------------------------------------
void Gibis::set_tipo(string tipo){
  this->tipo = tipo;
}
void Gibis::set_edicao(string edicao){
  this->tipo = edicao;
}
void Gibis::set_volume(int volume){
  this->volume = volume;
}
//-----------------GET----------------------------------------------------------
string Gibis::get_tipo(){
  return tipo;
}
string Gibis::get_edicao(){
  return edicao;
}
int Gibis::get_volume(){
  return volume;
}
//------------------------------methods-----------------------------------------
void Gibis::adicionaGibis(){
  fstream arquivo;

  arquivo.open("gibis.txt",fstream::out|fstream::app);
  if(arquivo.is_open()){
    arquivo << get_editora() << endl << get_titulo() << endl << get_quantidade() << endl << get_codigo() << endl << get_preco() << endl << get_tipo() << endl << get_edicao() << endl << get_volume() << endl << "--------------------------------------------------------------------------------------" << endl;
    cout << endl << endl << "Gibis add com sucesso! " << endl;
  }
  else
    cout << "ERRO" << endl;
  arquivo.close();

}
void Gibis::imprime(){
  cout << "TITULO: " << get_titulo() << endl;
  cout << "EDITORA: "<< get_editora() << endl;
  cout << "QUANTIDADE: "<< get_quantidade() << endl;
  cout << "CODIGO: " << get_codigo() << endl;
  cout << "PREÇO: " << get_preco() << endl;
  cout << "TIPO: " << get_tipo() << endl;
  cout << "EDIÇÃO: "<< get_edicao() << endl;
  cout << "VOLUME: "<< get_volume() << endl;
}

void Gibis::definetamanho(){
      ifstream arquivo;
      string quantidadegibis;
      arquivo.open("gibis.txt");
      if(arquivo.is_open()){
          getline(arquivo,quantidadegibis);
          this->quantidadegibis=atoi(quantidadegibis.c_str());
          arquivo.close();
      }else{
          this->quantidadegibis=0;
      }
  }
