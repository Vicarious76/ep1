#include "cliente.hpp"
#include <fstream>
#include <iostream>
#include <stdbool.h>
//--------------------------Builders--------------------------------------------
Cliente::Cliente(){
  nome = "Generico";
  cpf = "000.000.000-00";
  naturalidade = "Generico";
  endereco = "Generico";
  profissao = "Generico";
  idade = 0;
}

Cliente::Cliente(string nome, string cpf, string naturalidade, string endereco, string profissao, int idade){
  set_nome(nome);
  set_cpf(cpf);
  set_naturalidade(naturalidade);
  set_endereco(endereco);
  set_profissao(profissao);
  set_idade(idade);
}

//--------------Destructor------------------------------------------------------
Cliente::~Cliente(){
  cout << "Destrutor do objeto" << endl;
}
//-----------------SET----------------------------------------------------------
void Cliente::set_nome(string nome){
  this->nome = nome;
}
void Cliente::set_cpf(string cpf){
  this->cpf = cpf;
}
void Cliente::set_naturalidade(string naturalidade){
  this->naturalidade = naturalidade;
}
void Cliente::set_endereco(string endereco){
  this->endereco = endereco;
}
void Cliente::set_profissao(string profissao){
  this->profissao = profissao;
}
void Cliente::set_idade(int idade){
  this->idade = idade;
}
//-----------------GET----------------------------------------------------------
string Cliente::get_nome(){
  return nome;
}
string Cliente::get_cpf(){
  return cpf;
}
string Cliente::get_naturalidade(){
  return naturalidade;
}
string Cliente::get_endereco(){
  return endereco;
}
string Cliente::get_profissao(){
  return profissao;
}
int Cliente::get_idade(){
  return idade;
}
//------------------------------methods-----------------------------------------
void Cliente::adcionaCliente(){
  fstream arquivo;

  arquivo.open("cliente.txt",fstream::out|fstream::app);
  if(arquivo.is_open()){
    arquivo << get_nome() << endl << get_cpf() << endl << get_naturalidade() << endl << get_endereco() << endl << get_profissao() << endl << get_idade() << endl << "--------------------------------------------------------------------------------------"<< endl;
    cout << endl << endl << "Cliente add com sucesso! " << endl;
  }
  else
    cout << "ERRO!" << endl;
  arquivo.close();

}
void Cliente::imprime(){
  cout << "NOME: " << get_nome() << endl;
  cout << "CPF: " << get_cpf() << endl;
  cout << "NATURALIDADE: " << get_naturalidade() << endl;
  cout << "ENDEREÇO: "<< get_endereco() << endl;
  cout << "PROFISSÃO: "<< get_profissao() << endl;
  cout << "IDADE: "<< get_idade() << endl;
}
void Cliente::validaCliente(){
  ifstream arquivo;
  int contador = 0;
  string linha;
  arquivo.open("cliente.txt");
  if(arquivo.is_open()){
    while(getline(arquivo,linha)){
      if (linha == get_nome()){
         contador++;
      }
    }
    if(contador != 0){
      cout << "bem vindo" <<endl;
    }
    else {
      cout << "faça o cadastro do cliente " << endl;
    }
    arquivo.close();
  }
  else{
    cout << "erro!" << endl;
  }
}
