#include "livros.hpp"
#include <iostream>
#include <fstream>
#include <vector>
//--------------------------Builders--------------------------------------------
Livros::Livros(){
  autor = "Generico";
  datapublicacao = "Generico";
  assunto = "Generico";
}
Livros::Livros(string editora, string titulo, int quantidade, int codigo, float preco){
  set_editora(editora);
  set_titulo(titulo);
  set_quantidade(quantidade);
  set_codigo(codigo);
  set_preco(preco);
}
Livros::Livros(string editora, string titulo, int quantidade, int codigo, float preco, string autor, string datapublicacao, string assunto){
  set_autor(autor);
  set_datapublicacao(datapublicacao);
  set_assunto(assunto);
  set_editora(editora);
  set_titulo(titulo);
  set_quantidade(quantidade);
  set_codigo(codigo);
  set_preco(preco);
}
//--------------Destructor------------------------------------------------------
Livros::~Livros(){
  cout << "destrutor da classe" << endl;
}
//-----------------SET----------------------------------------------------------
void Livros::set_autor(string autor){
  this->autor = autor;
}
void Livros::set_datapublicacao(string datapublicacao){
  this->datapublicacao = datapublicacao;
}
void Livros::set_assunto(string assunto){
  this->assunto = assunto;
}
//-----------------GET----------------------------------------------------------
string Livros::get_autor(){
  return autor;
}
string Livros::get_datapublicacao(){
  return datapublicacao;
}
string Livros::get_assunto(){
  return assunto;
}

//------------------------------methods-----------------------------------------
void Livros::adicionaLivros(){
  fstream arquivo;

  arquivo.open("livros.txt",fstream::out|fstream::app);
  if(arquivo.is_open()){
    arquivo << get_editora() << endl << get_titulo() << endl << get_quantidade() << endl << get_codigo() << endl << get_preco() << endl << get_autor() << endl << get_datapublicacao() << endl << get_assunto() << endl << "--------------------------------------------------------------------------------------" << endl;
    cout << endl << endl << "Livros add com sucesso! " << endl;
  }
  else
    cout << "ERRO" << endl;
  arquivo.close();

}
void Livros::imprime(){
  cout << "TITULO: " << get_titulo() << endl;
  cout << "EDITORA: "<< get_editora() << endl;
  cout << "QUANTIDADE: "<< get_quantidade() << endl;
  cout << "CODIGO: " << get_codigo() << endl;
  cout << "PREÇO: " << get_preco() << endl;
  cout << "AUTOR: " << get_autor() << endl;
  cout << "PUBLICAÇÃO: "<< get_datapublicacao() << endl;
  cout << "ASSUNTO: "<< get_assunto() << endl;
}
void Livros::tiraproduto(){
  fstream arquivo;
/*
  arquivo.open("livros.txt",fstream::out);
  if(arquivo.open()){

  } */
}
