#include "carrinho.hpp"
#include <iostream>
//--------------------------Builders--------------------------------------------
Carrinho::Carrinho(){
  pagamento = 0;
}
Carrinho::Carrinho(float pagamento){
  set_pagamento(pagamento);
}
//--------------Destructor------------------------------------------------------
Carrinho::~Carrinho(){
  cout << "Destrutor do objeto" << endl;
}
//-----------------SET----------------------------------------------------------
void Carrinho::set_pagamento(float pagamento){
  this->pagamento = pagamento;
}
void Carrinho::set_desconto(float desconto){
  this->desconto = desconto;
}
//-----------------GET----------------------------------------------------------
float Carrinho::get_pagamento(){
  return pagamento;
}
float Carrinho::get_desconto(){
  return desconto;
}
//------------------------------methods-----------------------------------------

float Carrinho::calculadescontolivro(){
   return desconto = livro.get_preco() * 0.1;
}
float Carrinho::calculopagamentolivro(){
   return pagamento = livro.get_preco() - calculadescontolivro();
}
float Carrinho::pagamentorealLivro() {
   return valorfinal = calculopagamentolivro() * livro.get_quantidade();
}

float Carrinho::calculadescontogibis(){
  return desconto = gibis.get_preco() * 0.1;
}
float Carrinho::calculopagamentogibis(){
  return pagamento = gibis.get_preco() - calculadescontogibis();
}
float Carrinho::pagamentorealGibis(){
  return valorfinal = calculopagamentogibis() * gibis.get_quantidade();
}
