#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>
#include <vector>

using namespace std;

class Cliente{
private:
  //Attributes
  string nome, cpf, naturalidade, endereco, profissao;
  int idade;
  vector <Cliente*> cliente;

public:
  //builders
  Cliente();
  Cliente(string nome, string cpf, string naturalidade, string endereco, string profissao);
  Cliente(string nome, string cpf, string naturalidade, string endereco, string profissao, int idade);
  //destructor
  ~Cliente();
  //set
  void set_nome(string nome);
  void set_cpf(string cpf);
  void set_naturalidade(string naturalidade);
  void set_endereco(string endereco);
  void set_profissao(string profissao);
  void set_idade(int idade);
  //get
  string get_nome();
  string get_cpf();
  string get_naturalidade();
  string get_endereco();
  string get_profissao();
  int get_idade();
  //methods
  void adcionaCliente();
  void imprime();
  void validaCliente();




};
#endif
