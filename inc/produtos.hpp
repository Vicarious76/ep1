#ifndef PRODUTOS_HPP
#define PRODUTOS_HPP

#include <string>
#include <vector>

using namespace std;

class Produtos{
private:
  //Attributes
  int quantidade, codigo;
  string editora, titulo;
  float preco;

public:
  //builders
  Produtos();
  Produtos(int quantidade, int codigo, float preco);
  Produtos(string editora, string titulo, int quantidade, int codigo, float preco);
  //destructor
  ~Produtos();
  //sets
  void set_editora(string editora);
  void set_titulo(string titulo);
  void set_quantidade(int quantidade);
  void set_codigo(int codigo);
  void set_preco(float preco);
  //gets
  string get_editora();
  string get_titulo();
  int get_quantidade();
  int get_codigo();
  float get_preco();
  //methods
  void imprime();
  void tiraProduto();
  void tiraproduto();
  void definetamanho();
  void pegatamanho();

};
#endif
