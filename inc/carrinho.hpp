#ifndef CARRINHO_HPP
#define CARRINHO_HPP
#include "gibis.hpp"
#include "livros.hpp"

#include <string>

using namespace std;

class Carrinho{
private:
  //Attributes
   float pagamento;
   float desconto;
   float valorfinal;
   Gibis gibis;
   Livros livro;
public:
  //builders
  Carrinho();
  Carrinho(float pagamento);
  //destructor
  ~Carrinho();
  //set
  void set_pagamento(float pagamento);
  void set_desconto(float desconto);
  void set_valorfinal(float valorfinal);
  //get
  float get_pagamento();
  float get_desconto();
  float get_valorfinal();
  //methods
  float calculopagamentolivro();
  float calculadescontolivro();
  float pagamentorealLivro();
  float calculopagamentogibis();
  float calculadescontogibis();
  float pagamentorealGibis();

};
#endif
