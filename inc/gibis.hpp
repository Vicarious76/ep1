#ifndef GIBIS_HPP
#define GIBIS_HPP

#include <string>
#include "produtos.hpp"

using namespace std;

class Gibis: public Produtos{
private:
  //Attributes
  string tipo, edicao;
  int volume,quantidadegibis;

public:
  //builders
  Gibis();
  Gibis(int volume);
  Gibis(string tipo, string edicao, int volume);
  Gibis(string editora, string titulo, int quantidade, int codigo, float preco);
  Gibis(string editora, string titulo, int quantidade, int codigo, float preco, string tipo, string edicao, int volume);
  //destructor
  ~Gibis();
  //sets
  void set_tipo(string tipo);
  void set_edicao(string edicao);
  void set_volume(int volume);
  //gets
  string get_tipo();
  string get_edicao();
  int get_volume();
  //methods
  void adicionaGibis();
  void imprime();
  void tiraproduto();
  void definetamanho();
  void pegatamanho();

};
#endif
