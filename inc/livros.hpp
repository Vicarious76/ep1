#ifndef LIVROS_HPP
#define LIVROS_HPP

#include <string>
#include "produtos.hpp"

using namespace std;

class Livros: public Produtos{
private:
  //Attributes
  string autor, datapublicacao, assunto;
  int quantidadelivros;
  vector <Livros*> lista_livros;

public:
  //builders
  Livros();
  Livros(string autor, string datapublicacao, string assunto);
  Livros(string editora, string titulo, int quantidade, int codigo, float preco);
  Livros(string editora, string titulo, int quantidade, int codigo, float preco, string autor, string datapublicacao, string assunto);
  //destructor
  ~Livros();
  //sets
  void set_autor(string autor);
  void set_datapublicacao(string datapublicacao);
  void set_assunto(string assunto);
  //gets
  string get_autor();
  string get_datapublicacao();
  string get_assunto();
  //methods
  void adicionaLivros();
  void imprime();
  void tiraproduto();
  int definetamanho();

};
#endif
